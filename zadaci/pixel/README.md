# Pixel

> **NAPOMENA:**
> 
> Ovaj se zadatak bazira na zadatku 
> [Pixel](https://gitlab.com/pripreme/oop-kol1/-/tree/main/zadaci/pixel) iz prvog kolokvija.

Nekad nam pri radu s pikselima nije bitna njegova prozirnost, nego su nam dovoljne samo komponente boje (`red`, `green` i `blue`), no nekad trebamo i fleksibilnost koju prozirnost (`alpha`) pruža.<br>
Iz tog ćemo razloga našu klasu `Pixel` podijeliti na dva dijela.

## `RGB`
Klasa `RGB` predstavljat će piksel reprezentiran isključivo komponentama boja (`red`, `green` i `blue`). Za nju implementirajte konstuktore (*defaultni*, preopterećeni, *copy*), *gettere* i *settere* za komponente boja te metodu `to_grayscale` (*vidi zadatak iz prvog kolokvija za detalje ove metode*) i operator ispisa (vidi primjer ispisa za predloženi format).

## `RGBA`
Klasa `RGBA` proširuje reprezentaciju piksela klase `RGB` komponentom prozirnosti (`alpha`). Prepravite/doradite sve postojeće funkcionalnosti (preuzete iz klase `RGB`) ukoliko za to ima potrebe te dodajte one za koje smatrate da nedostaju.

## Main
U main dijelu programa kreirajte i ispišite jedan piksel bez, a potom jedan s komponentom prozironsti.
