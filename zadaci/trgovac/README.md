# Trgovac

Želimo pratiti rad trgovine, konkretno dvije najbitnije operacije blagajne - naplatu kupnje i zaključivanje na kraju radnog vremena.

`Trgovina` uz sebe ima vezan svoj `naziv` te `saldo`, odnosno iznos kojeg je zaradila vršeći prodaje (*razmisli: koliki je saldo nove trgovine?*).

## Naplata kupnje

Za radnju naplate kupnje potreban nam je kupac kojem ćemo tu kupnju naplatiti, stoga implementirajte klasu `Osoba` koja će imati `ime` i kojoj će biti pridružena određena `kolicina_novca`. Osim konstuktora, za osobu potrebno je implementirati *getter* za ime, metode za uplatu, odnosno isplatu određenog iznosa novca te metodu za provjeru ima li osoba dovoljno novca za plaćanje zadanog iznosa.

Kupcu možemo naplatiti kupnju samo ako on ima dovoljno novca kako bi tu kupnju platio (u našem scenariju ne može otići u minus). Tada kupac **daje svoj novac** u iznosu kojeg treba platiti, a njega potom **trgovina sprema**. <br>Kao rezultat radnje vratite informaciju o tome je li kupnja uspješno provedena.

**Savjet/prijedlog:** radi lakšeg praćenja kako se stvari (ne)odvijaju, u oba slučaja (kupac ima, odnosno nema dovoljno novca) unutar metode ispišite prigodnu poruku s informacijama o tome što se dogodilo (*vidi primjer ispisa za ideju*).


## Zaključivanje

Zaključivanje blagajne na kraju dana ne može odraditi bilo koja `Osoba`, nego jedino `Trgovac` - osoba koja radi u trgovini, odnosno ima informaciju o trgovini koja je njegov `poslodavac`.

Pri zaključivanju blagajne moramo još dodatno provjeriti radi li se o trgovcu koji je zaposlenik **naše** trgovine - je li `Trgovina` koju ima navedenu kao `poslodavac` upravo ona u kojoj trenutno zaključujemo blagajnu?
<details>
<summary><strong>Hint</strong> (probajte riješiti bez otvaranja):</summary> 

"Trgovina u kojoj trenutno zaključujemo blagajnu" je `this` pokazivač - treba usporediti tu memorijsku adresu s onom spremljenom kao `poslodavac` relevantnog trgovca.

</details>

Ako se radi o zaposleniku trgovine, potrebno je **uzeti novce trgovine** od tog dana i **prebaciti ih trgovcu**. <br>Kao rezultat radnje vratite informaciju o tome je li zaključivanje uspješno provedeno.

**Savjet/prijedlog:** kao i kod naplate kupnje, u oba slučaja (trgovac je, odnosno nije zaposlenik trgovine) unutar metode ispišite prigodnu poruku s informacijama o tome što se dogodilo (*vidi primjer ispisa za ideju*).


## Main

Kako bi dobili ispis jednak danom primjeru, potrebno je u main dijelu programa instancirati sljedeće podatke:
- dvije osobe:
  - `osoba1`, imena "A", s 10 novaca
  - `osoba2`, imena "B", s 250 novaca
- dvije trgovine:
  - `trgovina1`, naziva "Konzum"
  - `trgovina2`, naziva "Plodine"
- dva trgovca:
  - `trgovac1`, imena "T1", s 300 novaca, zaposlen u `trgovina1`
  - `trgovac2`, imena "T2", zaposlen u `trgovina2`

a potom izvršiti sljedeći niz radnji:
- u `trgovina1` (pokušati) naplatiti kupnju `osoba1` u iznosu 7
- u `trgovina1` (pokušati) naplatiti kupnju `osoba1` u iznosu 7
- u `trgovina1` (pokušati) naplatiti kupnju `osoba2` u iznosu 150
- u `trgovina1` (pokušati) naplatiti kupnju `trgovac1` u iznosu 150
- u `trgovina1` (pokušati) naplatiti kupnju `trgovac2` u iznosu 150
- `trgovac2` uplatiti 500 novaca
- u `trgovina1` (pokušati) naplatiti kupnju `trgovac2` u iznosu 150
- u `trgovina2` (pokušati) naplatiti kupnju `trgovac1` u iznosu 150
- `trgovina1` (pokušati) zaključiti od strane `trgovac1`
- `trgovina2` (pokušati) zaključiti od strane `trgovac1`
- `trgovina2` (pokušati) zaključiti od strane `trgovac2`
