# UI element

> **NAPOMENA:**
> 
> Ovaj je zadatak primarno zamišljen za rješavanje u Pythonu,
> ali se također možete okušati i u rješavanju koristeći C++.

Želimo napraviti program koji omogućuje kreiranje *korisničkih sučelja* (eng. *user interface* - **UI**).<br>Korisnička sučelja sastoje se od brojnih elemenata raspoređenih jedni uz/unutar drugih.

Prikaz sučelja simulirat ćemo na način da za svaki element ispišemo njegov naziv, a potom ono što taj element sadrži.

Elementi dolaze u dvije skupine:
- *container*, koji služe za organizaciju i raspored drugih elemenata
- *content*, koji služe za prikaz određenog sadržaja (npr. teksta, slika itd.)

## Implementacija
Klasa `UIElement` predstavljat će bazu za sve druge elemente. U njoj ćemo definirati osnovnu implementaciju pomoćne metode za vraćanje tekstualnog prikaza - vraćanje naziva elementa.

Dvije skupine/vrste elemenata predstavit ćemo klasama `UIContainer` i `UIContent`, svaka od kojih će naslijediti klasu `UIElement`.

### `UIContainer`
`UIContainer` služi za organizaciju drugih elemenata, stoga on prima (i sprema) sve elemente koje on "sadrži". Metodu za prikaz proširujemo na način da, osim dosadašnjeg prikaza naziva elementa, pozivamo metodu za prikaz i na svim elementima koje on sadrži kako bismo i njih prikazali (dakle prikazujemo njega i onda ispod sve "njegove pod-elemente").

Neki primjeri `UIContainer`a (klase koje ga nasljeđuju i predstavljaju konkretne elemente sučelja) su:
- `Navigation`
- `List`
- `Main`
- `Form`

#### Root
Poseban `UIContainer` je `Root` - on je početni (korijenski) element korisničkog sučelja i sadrži sve ostale elemente. Zbog toga na njemu dodatno definiramo metodu `render` koju ćemo koristiti kako bismo započeli proces prikaza svih elemenata.

### `UIContent`
`UIContent` služi za spremanje i prikaz konkretnih vrijednosti, stoga će on u metodi za prikaz nakon svog imena samo prikazati tu spremljenu vrijednost.

Neki primjeri `UIContent`a (klase koje ga nasljeđuju i predstavljaju konkretne elemente sučelja) su:
- `ListItem`
- `Heading`
- `Paragraph`
- `Label`
- `Button`

### Prikaz hijerarhije
Budući da su nam elementi organizirani na način da jedan element sadrži nekoliko drugih, ti elementi opet potencijalno sadrže neke treće, ti treći neke četvrte itd., prikazat ćemo to na način da je ispis svakog pod-elementa uvučen za dva razmaka više nego element koji ga sadrži (vidi primjer ispisa za vizualnu predodžbu).

### Napomene
Naziv elementa spremit ćemo u svakoj klasi kao atribut klase.

Prikaz elemenata bit će teško napraviti koristeći `print()` ili metodu `__str__` - umjesto njih napravite (ranije spomenutu) pomoćnu metodu u kojoj ćete "slagati" string za ispis, a onda u metodi `render` pozovite tu pomoćnu metodu (od korijenskog elementa) i *ovdje* taj rezultat ispišite koristeći `print()`.

[**Python only**] Ukoliko želimo argumente funkcije proslijediti zasebno, a unutar funkcije ih tretirati kao niz, možemo to napraviti tako da u funkciji primimo argument ispred koje je `*`, npr. `*argumenti` (više informacija možete pronaći [ovdje](https://realpython.com/python-kwargs-and-args/#using-the-python-args-variable-in-function-definitions)).

## Main
Potrebno je kombinirajući razne elemente napraviti strukturu prikaza (npr. prateći dani primjer ispisa) te nju prikazati koristeći `render` metodu korijenskog elementa.
