# Polaganje kolegija

Želimo napraviti program koji će olakšati praćenje statusa studenata, odnosno konkretno jesu li položili kolegije koje slušaju.

S obzirom da su načini polaganja kolegija višestruki, svaki od njih predstavit ćemo vlastitom klasom. <br>
Ono što možemo primjetiti je da svi imaju jednu zajedničku stvar: svi su u suštini nekakva provjera znanja koju mogu ili ne moraju položiti. Iz tog ćemo razloga napraviti klasu `ProvjeraZnanja` s metodom `je_polozeno`; one će nam služiti kao polazišna točka za ostale klase koje će predstavljati konkretne provjere znanja, odnosno načine polaganja kolegija.

Jedan od načina polaganja je (pismeni) `Ispit` - on predstavlja jednokratnu provjeru znanja na kojem je postavljen `prag_prolaznosti` i na kojem student ostvari određeni `rezultat` te polaže ukoliko je ostvareni rezultat veći od postavljenog praga.

Uz ispite, tu su još i `Kolokviji` kao oblik višekratne provjere znanja koje također imaju postavljen `prag_prolaznosti`, a studentovi `rezultati` su niz uspjeha ostvarenih na svakoj od pojedinačnih provjera.<br>
Ono što nije odmah jasno je kako utvrditi je li studentov uspjeh dovoljan za prolazak, odnosno polaganje provjere. Dva najčešća načina određivanja su `ZbrajajuciKolokviji`, gdje se studentu rezultati pojedinih provjera zbrajaju te se gleda je li prosječan rezultat veći od prolaznog praga, i `PojedinacniKolokviji` gdje student mora efektivno "položiti" svaku od provjera, odnosno gleda se je li svaki od ostvarenih rezultata veći od prolaznog praga.

Za kraj implementirajte klasu `Student` koja će sadržavati niz raznih provjera znanja, konstruktor(e), metodu za dodavanje nove provjere znanja u niz dosad spremljenih te metodu `je_polozio` koja će vratiti informaciju o tome je li student položio kolegij, odnosno je li položio ijednu provjeru znanja kojoj je pristupio za taj kolegij. 

## Main
U main dijelu programa kreirajte niz od nekoliko studenata, svaki od kojih ima jedan ili više konkretnih provjera znanja.<br>
Koristeći gotove funkcionalnosti iz biblioteke `algorithm`, prebrojite (i ispišite) koliko je od tih studenata položilo kolegij, koliki je postotak studenata položio, a potom te studente koji su položili prekopirajte u novi niz i provjerite (te ispišite) je li broj elemenata u tom nizu jednak originalnom broju studenata koji su položili kolegij.

### Primjer
Kako biste usporedili svoj rezultat s primjerom ispisa, napravite 3 studenta od kojih će:
1. pristupiti zbrajajućem kolokviju s pragom `50` i rezultatima `35, 70`
2. pristupiti
   - zbrajajućem kolokviju s pragom `45` i rezultatima `27.2, 40.1, 50.3`
   - ispitu s pragom `50` i rezultatom `47.8`
   - ispitu s pragom `50` i rezultatom `49.8`
3. pristupiti
   - pojedinačnom kolokviju s pragom `55` i rezultatima `73.8, 53.75`
   - ispitu s pragom `55` i rezultatom `63`

### Pomoć
Funkcija za kopiranje elemenata koji zadovoljavaju uvjet kao treći argument prima mjesto u nizu odakle će dodavati te kopije. To možemo najjednostavnije dobiti koristeći `back_inserter` funkciju kojoj prosljeđujemo niz u kojeg kopiramo. Dakle, ako npr. želimo podatke kopirati iz vektora `a` u vektor `b` prema uvjetu `p`, napravit ćemo to na sljedeći način:
```c++
copy_if(a.begin(), a.end(), back_inserter(b), p);
```

## Bonus

### 1. dio
Možemo primjetiti da sve provjere znanja dijele podatak o pragu prolaznosti - stoga ga, u svrhu boljeg iskorištavanja postojećeg koda, prebacite u klasu `ProvjeraZnanja` kao zaštičeni podatak te klasi dodajte **zaštićeni konstruktor** (pozivat će ga samo nasljeđene klase) uz pomoć kojeg ćemo taj podatak postavljati.


### 2. dio
Proširite klasu `Student` da umjesto provjere znanja za samo jedan kolegij, sadrži provjere znanja za sve kolegije koje taj student sluša. <br>
Shodno tome, proširite postojeće konstruktore i metodu za dodavanje provjere znanja kako bi primali informaciju o relevantnom kolegiju, prepravite postojeću metodu `je_polozio` tako da sada provjerava je li student položio **sve** kolegije, te dodatno preopteretite tu metodu tako da prima naziv kolegija (kao `string`) i provjerava je li student položio samo taj kolegij.

<details>
<summary><strong>Hint</strong> (probajte riješiti bez otvaranja):</summary> 

Podatke spremajte u `unordered_map` kojem će ključevi biti nazivi kolegija (`string`), a vrijednosti niz provjera znanja (`vector<ProvjeraZnanja*>`).

</details>
