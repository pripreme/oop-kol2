# Zadaci

Ovdje možete pronaći zadatke za pripremu, grupirane na način da je svaki u svojoj mapi/folderu/direktoriju.

> **PAŽNJA:**
> 
> Iznimka su direktoriji čiji naziv započinje s točkom (npr. `.assets`), 
> koji služe za interne i organizacijske svrhe.


Kako biste "otvorili" neki od zadataka, kliknite (jednom) na njegov naziv (uz ikonu mape/foldera/direktorija).

## Struktura zadatka

Za svaki zadatak dane su vam sljedeće datoteke:

- `README.md` - opis zadatka (koji vam je ispisan ispod popisa datoteka, slično kao i ovaj opis kojeg trenutno čitate)
- `ispis` - primjer kako izgleda ispis zadatka (format i rezultantni podaci), prema danom sadržaju `main` funkcije

> **SAVJET:**
> 
> Za olakšano kopiranje sadržaja datoteke (npr. predloška za implementaciju na svom stroju 
> ili podataka za usporedbu ispisa) <br> možete koristiti gumb u gornjem desnom kutu 
> ispisa sadržaja datoteke (klikom na njega, kopirat ćete sadržaj datoteke):
> 
> ![Gumb za kopiranje sadržaja datoteke](./.assets/img/copy-file-contents.png)

> **NAPOMENA:**
> 
> Neki zadaci uz navedene datoteke sadrže i pomoćne datoteke 
> (npr. vizuale, dodatne podatke, dodatan kod itd.)
> u svrhu boljeg pojašnjenja zadatka.
