# Točka

> **NAPOMENA:**
> 
> Ovaj se zadatak bazira na zadatku 
> [Točka](https://gitlab.com/pripreme/oop-kol1/-/tree/main/zadaci/tocka) iz prvog kolokvija.

Kako bismo unutar jednog programa mogli kreirati i dvodimenzionalne i trodimenzionalne točke, klasu `Tocka` rastavit ćemo na dva dijela.

## `Tocka2D`
`Tocka2D` predstavljat će točku u ravnini, s koordinatama `x` i `y`. Za nju implementirajte konstuktore (*defaultni*, preopterećeni, *copy*), *gettere* i *settere* za koordinate te metode za projekciju na x, odnosno y os. Konačno, implementirajte operator ispisa koji će ispisivati vrijednosti koordinata, odvojene zarezom.

## `Tocka3D`
**Ponovno upotrebljujući sve što možete iz klase `Tocka2D`**, implementirajte klasu `Tocka3D` koja će uz `x` i `y` sadržavati i `z` koordinatu. Prepravite/doradite sve postojeće funkcionalnosti (preuzete iz klase `Tocka2D`) ukoliko za to ima potrebe te dodajte one za koje smatrate da nedostaju.

## Main
U main dijelu programa kreirajte i ispišite jednu dvodimenzionalnu, a potom jednu trodimenzionalnu točku.
